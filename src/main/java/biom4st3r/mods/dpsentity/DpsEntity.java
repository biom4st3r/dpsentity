package biom4st3r.mods.dpsentity;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.GenericContainerScreenHandler;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;

import biom4st3r.mods.dpsentity.reflection.FieldRef.FieldHandler;
import io.netty.buffer.Unpooled;

public class DpsEntity extends MobEntity {

    static Class<?> AOE_DAMAGE;
    static FieldHandler<Float> AOE_SPECIAL_DAMAGE;

    static {
        try {
            AOE_DAMAGE = Class.forName("com.robertx22.age_of_exile.database.data.spells.spell_classes.bases.MyDamageSource");
            AOE_SPECIAL_DAMAGE = FieldHandler.get(AOE_DAMAGE, "realDamage", -1);
        } catch (ClassNotFoundException e) {}
    }

    public DpsEntity(EntityType<? extends MobEntity> entityType, World world) {
        super(entityType, world);
    }

    @Override
    public boolean canPickUpLoot() {
        return true;
    }

    Inventory inventory = new SimpleInventoryExtension(this, 9);

    @Override
    protected ActionResult interactMob(PlayerEntity player, Hand hand) {
        if(!player.world.isClient) {
            player.openHandledScreen(new NamedScreenHandlerFactory(){

                @Override
                public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
                    return new GenericContainerScreenHandler(ScreenHandlerType.GENERIC_9X1, syncId, player.inventory, inventory, 1);
                }

                @Override
                public Text getDisplayName() {
                    return new LiteralText("Feet | Legs | Chest | Helm | MainHand | OffHand");
                }
                
            });
        }
        return ActionResult.SUCCESS;
    }

    @Override
    public void pushAwayFrom(Entity entity) {
        // super.pushAwayFrom(entity);
    }

    @Override
    protected void pushAway(Entity entity) {
        // super.pushAway(entity);
    }

    @Override
    public void kill() {
        this.remove();
    }

    @Override
    public void setVelocity(Vec3d velocity) {
        // SNIP
    }

    //#region tracking
    float lastDamage;
    float dps;
    Vec3d knockback = Vec3d.ZERO;
    float highestDps;
    public void setVals(float lastDamage, float dps, Vec3d knockback, float highestDps) {
        this.lastDamage = lastDamage;
        this.dps = dps;
        this.knockback = knockback;
        this.highestDps = highestDps;
    }

    float trackingDamage;
    long last;

    @Override
    public void addVelocity(double deltaX, double deltaY, double deltaZ) {
        this.knockback = new Vec3d(deltaX, deltaY, deltaZ);
    }

    public void dataPacket() {
        if(this.world.isClient) return;
        PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
        buf.writeInt(this.getEntityId());
        buf.writeFloat(lastDamage);
        buf.writeFloat(dps);
        if(knockback == null) {
            buf.writeDouble(0);
            buf.writeDouble(0);
            buf.writeDouble(0);
        } else {
            buf.writeDouble(knockback.x);
            buf.writeDouble(knockback.y);
            buf.writeDouble(knockback.z);
        }
        buf.writeFloat(highestDps);
        ServerWorld world = (ServerWorld) this.world;
        List<ServerPlayerEntity> list = world.getPlayers(player->this.getPos().subtract(player.getPos()).length() < 25);
        list.forEach(player-> {
            ServerPlayNetworking.send(player, ModInitClient.PACKET, buf);
        });
    }

    private void damageTrackingTick() {
        if(System.nanoTime() - last > 1E9) {
            dps = trackingDamage;
            trackingDamage = 0;
            if(highestDps < dps) {
                highestDps = dps;
            }
            last = System.nanoTime();
        }
    }
    int i = 0;
    @Override
    public void tick() {
        super.tick();
        if((i++ & 5) == 0 && !this.world.isClient) {
            damageTrackingTick();
            dataPacket();
        }
    }

    @Override
    public void setHealth(float health) {
        if(prev == null) return;
        float amount = this.getHealth()-health;

        if(AOE_DAMAGE != null && AOE_DAMAGE.isInstance(prev)) {
            amount = AOE_SPECIAL_DAMAGE.get(prev);
        }
        this.lastDamage = amount;
        this.trackingDamage += amount;
        dataPacket();
    }
    
    DamageSource prev;

    @Override
    protected void applyDamage(DamageSource source, float amount) {
        prev = source;
        super.applyDamage(source, amount);
    }

    @Override
    public void takeKnockback(float f, double d, double e) {
        this.knockback = new Vec3d(f, d, e);
    }

    @Override
    public boolean hasCustomName() {
        return false;
    }
    //#endregion

}

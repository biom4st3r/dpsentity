package biom4st3r.mods.dpsentity;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.LivingEntityRenderer;
import net.minecraft.client.render.entity.feature.ArmorFeatureRenderer;
import net.minecraft.client.render.entity.feature.HeldItemFeatureRenderer;
import net.minecraft.client.render.entity.model.BipedEntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.StringVisitable;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Matrix4f;

public class DpsEntityRenderer extends LivingEntityRenderer<DpsEntity, DpsEntityModel> {

    @SuppressWarnings({"unchecked","rawtypes"})
    public DpsEntityRenderer(EntityRenderDispatcher dispatcher, DpsEntityModel model, float shadowRadius) {
        super(dispatcher, model, shadowRadius);
        this.addFeature(new ArmorFeatureRenderer(this, new BipedEntityModel(0.5F), new BipedEntityModel(1.0F)));
        this.addFeature(new HeldItemFeatureRenderer(this));
    }

    public static final Identifier TEXTURE = new Identifier("textures/entity/armorstand/wood.png");

    @Override
    public Identifier getTexture(DpsEntity entity) {
        return TEXTURE;
    }

    @Override
    public void render(DpsEntity entity, float f, float g, MatrixStack stack,
            VertexConsumerProvider vertexConsumerProvider, int i) {
        super.render(entity, f, g, stack, vertexConsumerProvider, i);
        float pos = 0.85F;
        renderLabel(entity, new LiteralText(String.format("Last damage: %.3f", entity.lastDamage)), stack, vertexConsumerProvider, i, pos+=0.25);
        renderLabel(entity, new LiteralText(String.format("dps: %.3f", entity.dps)), stack, vertexConsumerProvider, i, pos+=0.25);
        renderLabel(entity, new LiteralText(String.format("knockback: (%.3f | %.3f | %.3f)", entity.knockback.x, entity.knockback.y, entity.knockback.z)), stack, vertexConsumerProvider, i, pos+=0.25);
        renderLabel(entity, new LiteralText(String.format("kb vector: %.4f", entity.knockback.length())), stack, vertexConsumerProvider, i, pos+=0.25);
        renderLabel(entity, new LiteralText(String.format("highest dps: %.3f", entity.highestDps)), stack, vertexConsumerProvider, i, pos+=0.25);
    }

    @SuppressWarnings({"resource"})
    protected void renderLabel(DpsEntity entity, Text text, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, float additionalHeight) {
        double d = this.dispatcher.getSquaredDistanceToCamera(entity);
        if (d <= 4096.0D) {
           boolean bl = !entity.isSneaky();
           float f = entity.getHeight() + additionalHeight;
           matrices.push();
           matrices.translate(0.0D, (double)f, 0.0D);
           matrices.multiply(this.dispatcher.getRotation());
           matrices.scale(-0.025F, -0.025F, 0.025F);
           Matrix4f matrix4f = matrices.peek().getModel();
           float g = MinecraftClient.getInstance().options.getTextBackgroundOpacity(0.25F);
           int j = (int)(g * 255.0F) << 24;
           TextRenderer textRenderer = this.getFontRenderer();
           float h = (float)(-textRenderer.getWidth((StringVisitable)text) / 2);
           textRenderer.draw(text, h, 0, 553648127, false, matrix4f, vertexConsumers, bl, j, light);
           if (bl) {
              textRenderer.draw((Text)text, h, 0, -1, false, matrix4f, vertexConsumers, false, 0, light);
           }
  
           matrices.pop();
        }
     }
}

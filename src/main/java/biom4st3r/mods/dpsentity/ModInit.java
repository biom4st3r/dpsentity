package biom4st3r.mods.dpsentity;

import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.item.Item;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;

public class ModInit implements ModInitializer
{
	public static final String MODID = "dpsentity";

	public static final EntityType<?> TYPE = FabricEntityTypeBuilder 
		.createMob()
		.defaultAttributes(()-> {
			return MobEntity.createMobAttributes().add(EntityAttributes.GENERIC_MAX_HEALTH, 10_000D);
		})
		.spawnGroup(SpawnGroup.CREATURE)
		.dimensions(EntityDimensions.fixed(0.6F, 1.95F))
		.trackRangeBlocks(10)
		.entityFactory(DpsEntity::new)
		.build();

	@Override
	public void onInitialize() {
		Registry.register(Registry.ITEM, new Identifier(MODID,"dpsentity"), new SpawnEggItem(TYPE, 0xFFFF_FFFF, 0x0000_0000, new Item.Settings()));
		Registry.register(Registry.ENTITY_TYPE, new Identifier(MODID,"entity"), TYPE);
	}
	
}

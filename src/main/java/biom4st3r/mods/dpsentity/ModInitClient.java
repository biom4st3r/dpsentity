package biom4st3r.mods.dpsentity;

import net.minecraft.util.Identifier;
import net.minecraft.util.math.Vec3d;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;

/**
 * ModInitClient
 */
public class ModInitClient implements ClientModInitializer {

    public static Identifier PACKET = new Identifier(ModInit.MODID,"packet");

    @Override
    public void onInitializeClient() {
        EntityRendererRegistry.INSTANCE.register(ModInit.TYPE, (manager,context)-> {
            return new DpsEntityRenderer(manager, new DpsEntityModel(1), 1);
        });
        ClientPlayNetworking.registerGlobalReceiver(PACKET, (client, handler, buf, responseSender)-> {
            int entityId = buf.readInt();
            float lastDamage = buf.readFloat();
            float dps = buf.readFloat();
            Vec3d kb = new Vec3d(buf.readDouble(), buf.readDouble(), buf.readDouble());
            float highestDps = buf.readFloat();
            client.execute(()-> {
                DpsEntity entity = (DpsEntity) client.world.getEntityById(entityId);
                entity.setVals(lastDamage, dps, kb, highestDps);
            });
        });
    }

    
}
package biom4st3r.mods.dpsentity;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.collection.DefaultedList;

final class SimpleInventoryExtension extends SimpleInventory {
    /**
     *
     */
    private final DpsEntity dpsEntity;

    SimpleInventoryExtension(DpsEntity dpsEntity, int size) {
        super(size);
        this.dpsEntity = dpsEntity;
    }

    private DefaultedList<ItemStack> armor() {
        return (DefaultedList<ItemStack>) this.dpsEntity.getArmorItems();
    }

    public ItemStack getStack(int slot) {
        switch(slot) {
            case 0:
            case 1:
            case 2:
            case 3:
                return armor().get(slot);
            case 4:
                return this.dpsEntity.getMainHandStack();
            case 5:
                return this.dpsEntity.getOffHandStack();
            default:
                return ItemStack.EMPTY;
        }
    }

    public void setStack(int slot, ItemStack stack) {
        switch(slot) {
            case 0:
            case 1:
            case 2:
            case 3:
                armor().set(slot, stack);
                break;
            case 4:
                this.dpsEntity.equipStack(EquipmentSlot.MAINHAND, stack);
                break;
            case 5:
                this.dpsEntity.equipStack(EquipmentSlot.OFFHAND, stack);
                break;
            default:
                break;
        }
    }

    public ItemStack removeStack(int slot) {
        switch(slot) {
            case 0:
            case 1:
            case 2:
            case 3: 
                return armor().set(slot,ItemStack.EMPTY);
            case 4:
                ItemStack stack = this.dpsEntity.getMainHandStack();
                this.dpsEntity.equipStack(EquipmentSlot.MAINHAND, ItemStack.EMPTY);
                return stack;
            case 5:
                ItemStack stack0 = this.dpsEntity.getOffHandStack();
                this.dpsEntity.equipStack(EquipmentSlot.OFFHAND, ItemStack.EMPTY);
                return stack0;
            default:
                return ItemStack.EMPTY;
        }
    }

    public ItemStack removeStack(int slot, int amount) {
        return this.removeStack(slot);
    }

    public boolean isEmpty() {
        return false;
    }

    public ItemStack addStack(ItemStack stack) {
        for(int i = 0; i < 4; i++) {
            if(armor().get(i).isEmpty()) {
                this.setStack(i, stack);
            }
        }
        return stack;
    }

    public int size() {
        return 9;
    }
}